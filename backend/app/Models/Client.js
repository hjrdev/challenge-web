'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Client extends Model {
  file () { return this.belongsTo('App/Models/File') }

  projects () { return this.hasMany('App/Models/Project') }
}

module.exports = Client
