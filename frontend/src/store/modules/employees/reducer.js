const initialState = {
  data: [],
};

export default function user(state = initialState, action) {
  switch (action.type) {
    case '@employee/GET_SUCCESS':
      return { ...state, data: action.payload.data };

    case '@employee/DELETE_REQUEST':
      return {
        ...state,
        data: state.data.filter(employee => employee.id !== action.id),
      };

    default:
      return state;
  }
}
