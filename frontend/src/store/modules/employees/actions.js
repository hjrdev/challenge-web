export function getEmployeesRequest() {
  return {
    type: '@employee/GET_REQUEST',
  };
}
export function getEmployeesSuccess(data) {
  return {
    type: '@employee/GET_SUCCESS',
    payload: { data },
  };
}

export function createEmployeeRequest(data) {
  return {
    type: '@employee/CREATE_REQUEST',
    payload: { data },
  };
}
export function updateEmployeeRequest(id, data) {
  return {
    type: '@employee/UPDATE_REQUEST',
    payload: { id, data },
  };
}

export function deleteEmployeeRequest(id) {
  return {
    type: '@employee/DELETE_REQUEST',
    payload: { id },
  };
}
