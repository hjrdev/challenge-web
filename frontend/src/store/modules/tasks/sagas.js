import { takeLatest, call, put, all } from 'redux-saga/effects';
import { toast } from 'react-toastify';

import api from '~/services/api';
import history from '~/services/history';

import { getProjectTasksRequest } from './actions';

export function* getTasks({ payload }) {
  try {
    const { id } = payload;
 
    console.log('Sagas -> projectId', id)

    const response = yield call(api.get, `projects/${id}/tasks`);

    yield put(getProjectTasksRequest(response.data));
  } catch (error) {
    toast.error('Problemas na listagens das tarefas');
  }
}

export function* createTask({ payload }) {
  try {
    const { data } = payload;

    yield call(api.post, `projects`, data);

    toast.success('Tarefa criada com sucesso');
  } catch (error) {
    toast.error('Falha: Tarefa não foi criada');
  }
}

export function* updateTask({ payload }) {
  try {
    const { id, data } = payload;

    yield call(api.put, `projects/${id}`, data);

    toast.success('Tarefa atualizada com sucesso');
  } catch (error) {
    toast.error('Falha: Tarefa não foi atualizada');
  }
}

export function* deleteTask({ payload }) {
  try {
    const { id } = payload;

    yield call(api.delete, `projects/${id}`);

    toast.success('Tarefa deletada com sucesso');
    history.push('/tasks');
  } catch (error) {
    toast.error('Falha: Tarefa não foi removida');
  }
}

export default all([
  takeLatest('@task/GET_REQUEST', getTasks),
  takeLatest('@task/DELETE_REQUEST', deleteTask),
  takeLatest('@task/UPDATE_REQUEST', updateTask),
  takeLatest('@task/CREATE_REQUEST', createTask),
]);
