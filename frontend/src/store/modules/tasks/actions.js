
export function getProjectTasksRequest(id) {
  return {
    type: '@task/GET_REQUESTPROJECT',
    payload: { id },
  };
}

export function getTasksRequest() {
  return {
    type: '@task/GET_REQUEST',
  };
}

export function getTasksSuccess(data) {
  return {
    type: '@task/GET_SUCCESS',
    payload: { data },
  };
}

export function createTaskRequest(data) {
  return {
    type: '@task/CREATE_REQUEST',
    payload: { data },
  };
}
export function updateTaskRequest(id, data) {
  return {
    type: '@task/UPDATE_REQUEST',
    payload: { id, data },
  };
}

export function deleteTaskRequest(id) {
  return {
    type: '@task/DELETE_REQUEST',
    payload: { id },
  };
}
