export function getClientsRequest() {
  return { type: '@client/GET_REQUEST' };
}

export function getClientsSuccess(data) {
  return {
    type: '@client/GET_SUCCESS',
    payload: { data },
  };
}

export function createClientRequest(data) {
  return {
    type: '@client/CREATE_REQUEST',
    payload: { data },
  };
}

export function updateClientRequest(id, data) {
  return {
    type: '@client/UPDATE_REQUEST',
    payload: { id, data },
  };
}

export function deleteClientRequest(id) {
  return {
    type: '@client/DELETE_REQUEST',
    payload: { id },
  };
}
