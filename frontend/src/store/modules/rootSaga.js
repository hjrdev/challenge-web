import { all } from 'redux-saga/effects';

import auth from './auth/sagas';
import user from './user/sagas';
import projects from './projects/sagas';
import clients from './clients/sagas';
import employees from './employees/sagas';
import tasks from './tasks/sagas'; 

export default function* rootSaga() {
  return yield all([auth, user, projects, clients, employees, tasks]);
}
