export function getProjectsRequest(search) {
  return {
    type: '@project/GET_REQUEST',
    payload: { search },
  };
}

export function getProjectsSuccess(data) {
  return {
    type: '@project/GET_SUCCESS',
    payload: { data },
  };
}

export function createProjectRequest(data) {
  return {
    type: '@project/CREATE_REQUEST',
    payload: { data },
  };
}
export function updateProjectRequest(id, data) {
  return {
    type: '@project/UPDATE_REQUEST',
    payload: { id, data },
  };
}

export function deleteProjectRequest(id) {
  return {
    type: '@project/DELETE_REQUEST',
    payload: { id },
  };
}
