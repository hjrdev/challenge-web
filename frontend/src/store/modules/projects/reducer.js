const initialState = {
  data: [],
};

export default function user(state = initialState, action) {
  switch (action.type) {
    case '@project/GET_SUCCESS':
      return {
        ...state,
        data: action.payload.data,
      };

    case '@project/DELETE_REQUEST':
      const { id } = action.payload;
      return {
        ...state,
        data: state.data.filter(project => project.id !== id),
      };

    default:
      return state;
  }
}
