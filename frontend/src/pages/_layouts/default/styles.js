import styled from 'styled-components';

export const Wrapper = styled.div`
  min-height: 100%;
  background: #eee;

  table {
    color: #444;
    width: 100%;
    border-collapse: collapse;

    thead {
      tr {
        margin-bottom: 20px;
        text-align: left;
      }

      th {
        padding-bottom: 10px;
        text-transform: uppercase;
      }
    }

    tbody {
      tr {
        border-top: 1px solid #ddd;
        background: #fff;
        transition: all 1s ease;

        &:hover {
          background: #eee;
          transition: all 0.25s ease;
          padding-left: 10px;
        }
      }

      td {
        padding: 5px 5px;
        transition: all 0.5s ease;

        &:last-of-type {
          text-align: right;
        }
      }

      a {
        color: #4d85ee;
      }
    }
  }
`;
