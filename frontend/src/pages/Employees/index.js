import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { FaPlus } from 'react-icons/fa';

import NotFound from '~/components/NotFound';
import Modal from '~/components/Modal';
import api from '~/services/api';

import { Container, Header } from './styles';
import {
  getEmployeesRequest,
  deleteEmployeeRequest,
} from '~/store/modules/employees/actions';

export default function Employees() {
  const dispatch = useDispatch();

  const [employees, setEmployees] = useState([]);
  const [selectEmployee, setSelectEmployee] = useState(null);

  const [openModal, setOpenModal] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    async function getEmployees() {
      const response = await api.get('users');

      const data = response.data.map(employee => ({
        ...employee
      }));

      setEmployees(data);
    }

    getEmployees();
  }, []);

  useEffect(() => {
    dispatch(getEmployeesRequest());
  }, [dispatch]);

  useEffect(() => {
    setOpenModal(false);
  }, [employees]);

  function closeModalForm() {
    setOpenModal(false);
  }

  function openModalForm(employee) {
    setSelectEmployee(employee);
    setOpenModal(true);
  }

  function handleSubmit() {
    const { id } = selectEmployee;

    dispatch(deleteEmployeeRequest(id));

    setLoading(true);

    setOpenModal(false);
  }

  if (loading) {
    return <div />;
  }

  return (
    <Container>
      <Header>
        <h1>Gerenciando colaboradores</h1>
        <Link to="/employees/new">
          <FaPlus size={14} color="#fff" />
          <label>CADASTRAR</label>
        </Link>
      </Header>

      <section>
        {employees.length > 0 ? (
          <table>
            <thead>
              <tr>
                <th>Nome</th>
                <th>Email</th>
                <th colSpan="2">&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              {employees.map(employee => (
                <tr key={employee.id}>
                  <td>{employee.username}</td>
                  <td>{employee.email}</td>
                  <td>
                    <Link to={`/employees/${employee.id}/edit`}>
                      editar
                    </Link>
                    <button
                      type="button"
                      onClick={() => openModalForm(employee)}
                    >
                      apagar
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          <NotFound />
        )}
      </section>

      {openModal && (
        <Modal>
          <p>
            Deseja deletar a matrícula:{' '}
            <strong>#{selectEmployee.id}</strong>?
          </p>
          <button type="button" onClick={handleSubmit}>
            Confirmar
          </button>
          <button type="button" onClick={closeModalForm}>
            Cancelar
          </button>
        </Modal>
      )}
    </Container>
  );
}
